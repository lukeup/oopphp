<html>
<head>
<title><?php echo "Object Oriented Programming in PHP"; ?></title>
</head>
<body>


<?php

class Animal implements Singable
{

    protected $name;

    protected $favourite_food;

    protected $sound;

    protected $id;

    public static $numberOfAnimals = 0;

    const PI = "3.1415";

    function getName()
    {
        return $this->name;
    }

    function __construct()
    {
        $this->id = rand(10, 10000);
        echo $this->id . " has been assigned <br />";

        Animal::$numberOfAnimals ++;
    }

    public function __destruct()
    {
        echo $this->name . " has been destroyed <br />";
    }

    function __get($name)
    {
        echo "Asked for " . $name . "<br />";
        return $this->$name; 
        //This will be the name, that was pasted here (in function parameter so $this->$name, not $this->name
    }

    function __set($name, $value)
    {
        switch ($name) {
            case "name":
                $this->name = $value;
                break;
            case "favourite_food":
                $this->favourite_food = $value;
                break;
            case "sound":
                $this->sound = $value;
                break;

            default:
                echo $name . " not found";
        }

        echo "Set " . $name . " to " . $value . "<br />";
    }
    
    function run()
    {
        echo $this->name . " runs<br/>";
    }
    
    final function whatIsGood(){
        echo "Running is good<br/>";
    }
    
    function __toString(){
        return $this->name . " says " . $this->sound . ", give me some " . $this->favourite_food . ", my id is " . $this->id . ".<br/>Total animals = ". Animal::$numberOfAnimals . "<br/><br/>";
        
    }
    function sing()
    {
        echo $this->name . " sings 'how how'<br/>";
    }
    static function addThese($num1, $num2){
        return ($num1+$num2)."<br/>";
    }
    //__call() provide method overloading
}

class Dog extends Animal implements Singable
{
    function run()
    {
        echo $this->name . " runs very fast<br/>";
    }
    function sing(){
        echo $this->name . " sings 'Wrr'<br/>";
       
    }
    
}

interface Singable{
    public function sing();
}

//Abstract class/method cannot be instantiated, but it forces to override it
abstract class RandomClass {
    abstract function RandomFunction($param);
}

$animalOne = new Animal();
$animalOne->name = "Piko";
$animalOne->favourite_food = "Meat";
$animalOne->sound = "Wark";

echo $animalOne->name . " says " . $animalOne->sound . ", give me some " . $animalOne->favourite_food . ", my id is " . $animalOne->id . ".<br/>Total animals = ". Animal::$numberOfAnimals . "<br/><br/>";

$animalTwo = new Dog();
$animalTwo->name = "Spot";
$animalTwo->favourite_food = "Beaf";
$animalTwo->sound = "Ruff";

echo $animalTwo->name . " says " . $animalTwo->sound . ", give me some " . $animalTwo->favourite_food . ", my id is " . $animalTwo->id . ".<br/>Total animals = ". Dog::$numberOfAnimals . "<br/><br/>";

$animalOne->run();
$animalTwo->run();
$animalTwo->whatIsGood();

echo $animalOne; //from magic method __toString()
echo $animalTwo; //from magic method __toString()

$animalOne->sing();
$animalTwo->sing();

//This is also very good explanation of polymorphysm. It works because they are both (Animal obj and Dog obj) type of Singable
function makeThemSing(Singable $singingAnimal){
    $singingAnimal->sing();
}
makeThemSing($animalOne);
makeThemSing($animalTwo);

//static functions don't need an object
echo "1+5 = " . Animal::addThese(1, 5). "<br/>";

$isItAnAnimal = ($animalTwo instanceof Animal) ? "TRUE" : "FALSE";
echo "check: ".$isItAnAnimal ."<br/>";

$isItAnAnimal = ($animalOne instanceof Dog) ? "TRUE" : "FALSE";
echo "check: ".$isItAnAnimal ."<br/>";

$animalThree = clone $animalOne; //
echo $animalThree;


?>
</body>
</html>